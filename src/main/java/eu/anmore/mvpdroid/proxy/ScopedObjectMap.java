package eu.anmore.mvpdroid.proxy;

import java.util.HashMap;

import com.google.inject.Key;

/**
 * @author Grzegorz Lipecki
 */
public class ScopedObjectMap extends HashMap<Key<?>, Object> {

	/** Serialization UID */
	private static final long serialVersionUID = 569241873998310234L;

}
