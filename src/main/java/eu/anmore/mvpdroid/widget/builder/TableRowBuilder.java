package eu.anmore.mvpdroid.widget.builder;

import android.app.Activity;
import android.widget.TableRow;

public final class TableRowBuilder extends ViewBuilder<TableRow> {

	public TableRowBuilder(final Activity context) {
		super(context, TableRow.class);
	}

}