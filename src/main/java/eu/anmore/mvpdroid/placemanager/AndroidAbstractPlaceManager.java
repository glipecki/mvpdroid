package eu.anmore.mvpdroid.placemanager;

import java.util.Map;
import java.util.Map.Entry;

import roboguice.inject.ContextSingleton;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.inject.Inject;

/**
 * @author Grzegorz Lipecki
 */
@ContextSingleton
public class AndroidAbstractPlaceManager implements AndroidPlaceManager {

	@Inject
	public AndroidAbstractPlaceManager(final Activity context) {
		this.context = context;
		this.gson = new Gson();
	}

	protected void startActivity(final Class<? extends Activity> activityClass) {
		final Intent activityIntent = new Intent(context, activityClass);
		context.startActivity(activityIntent);
	}

	protected void startActivity(final Class<? extends Activity> activityClass, final int flags) {
		final Intent activityIntent = new Intent(context, activityClass);
		activityIntent.setFlags(flags);
		context.startActivity(activityIntent);
	}

	// TODO (glipecki): refactor to some Builder object instead of Map. Builder
	// with more possible parameters, without Android dependencies, which can be
	// build by static factory methods of concrete Proxies.
	protected void startActivity(final Class<? extends Activity> activityClass, final Map<String, String> paramters) {
		final Intent activityIntent = new Intent(context, activityClass);
		for (final Entry<String, String> parameter : paramters.entrySet()) {
			activityIntent.putExtra(parameter.getKey(), parameter.getValue());
		}
		context.startActivity(activityIntent);
	}

	protected void startActivityJson(final Class<? extends Activity> activityClass, final Map<String, Object> paramters) {
		final Intent activityIntent = new Intent(context, activityClass);
		for (final Entry<String, Object> parameter : paramters.entrySet()) {
			activityIntent.putExtra(parameter.getKey(), asJson(parameter.getValue()));
		}
		context.startActivity(activityIntent);
	}

	private String asJson(final Object parameter) {
		return gson.toJson(parameter);
	}

	private final Context context;

	private final Gson gson;

}
