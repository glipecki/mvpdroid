package eu.anmore.mvpdroid.module;

import com.google.inject.AbstractModule;

/**
 * @author Grzegorz Lipecki
 */
public abstract class AndroidModule extends AbstractModule {

}
