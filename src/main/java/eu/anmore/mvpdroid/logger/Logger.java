package eu.anmore.mvpdroid.logger;

/**
 * @author Grzegorz Lipecki
 */
public interface Logger {

	public void trace(final String message);

	public void trace(final String message, final Object... params);

	public void info(final String message);

	public void info(final String message, final Object... params);

	public void debug(final String message);

	public void debug(final String message, final Object... params);

	public void warn(final String message);

	public void warn(final String message, final Object... params);

	public void error(final String message);

	public void error(final String message, final Object... params);

}
