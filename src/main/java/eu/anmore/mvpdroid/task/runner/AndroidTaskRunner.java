package eu.anmore.mvpdroid.task.runner;

import java.util.HashMap;
import java.util.Map;

import roboguice.inject.ContextSingleton;
import roboguice.util.SafeAsyncTask;
import android.app.Activity;

import com.google.inject.Inject;

import eu.anmore.mvpdroid.async.TaskAction;
import eu.anmore.mvpdroid.async.TaskActionHandler;
import eu.anmore.mvpdroid.async.TaskCallback;
import eu.anmore.mvpdroid.async.TaskResult;
import eu.anmore.mvpdroid.injector.Injector;
import eu.anmore.mvpdroid.logger.LogCatLogger;
import eu.anmore.mvpdroid.logger.Logger;

/**
 * @author Grzegorz Lipecki
 */
@ContextSingleton
public abstract class AndroidTaskRunner implements TaskRunner {

	@Inject
	public AndroidTaskRunner(final Activity context) {
		this.context = context;
		taskActionHandlers = new HashMap<Class<? extends TaskAction>, Class<? extends TaskActionHandler<? extends TaskAction, ? extends TaskResult>>>();
		configure();
	}

	@Override
	public <TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>> void execute(
			final TA taskAction, final TC taskCallback) {
		execute(taskAction, taskCallback, false);
	}

	@Override
	public <TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>> void executeWithRetry(
			final TA taskAction, final TC taskCallback) {
		execute(taskAction, taskCallback, true);
	}

	protected abstract void configure();

	protected Binder bindAction(final Class<? extends TaskAction> taskActionClass) {
		return new Binder(taskActionClass);
	}

	protected class Binder {

		public void toHandler(
				final Class<? extends TaskActionHandler<? extends TaskAction, ? extends TaskResult>> taskActionHandlerClass) {
			taskActionHandlers.put(taskActionClass, taskActionHandlerClass);
		}

		private Binder(final Class<? extends TaskAction> taskActionClass) {
			this.taskActionClass = taskActionClass;
		}

		private final Class<? extends TaskAction> taskActionClass;

	}

	private static final Logger log = LogCatLogger.getLogger(AndroidTaskRunner.class);

	static private class TaskRunner<TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>> extends
			SafeAsyncTask<Void> {

		public TaskRunner(final Activity presenter, final TaskActionHandler<TA, TR> taskActionHandler,
				final TA taskAction, final TC taskCallback) {
			this.presenter = presenter;
			this.taskActionHandler = taskActionHandler;
			this.taskAction = taskAction;
			this.taskCallback = taskCallback;
		}

		@Override
		public Void call() throws Exception {
			try {
				executeCall();
			} catch (final Throwable caught) {
				onFailure(caught);
				throw new RuntimeException(caught);
			}
			return null;
		}

		protected void executeCall() {
			final TR taskResult = taskActionHandler.handle(taskAction);
			onSuccess(taskResult);
		}

		protected void onSuccess(final TR taskResult) {
			presenter.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					taskCallback.onSuccess(taskResult);
				}
			});
		}

		protected void onFailure(final Throwable caught) {
			presenter.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					taskCallback.onFailure(caught);
				}
			});
		}

		private final Activity presenter;

		private final TaskActionHandler<TA, TR> taskActionHandler;

		private final TA taskAction;

		private final TC taskCallback;

	}

	static private class RetryTaskRunner<TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>>
			extends TaskRunner<TA, TR, TC> {

		public RetryTaskRunner(final Activity presenter, final TaskActionHandler<TA, TR> taskActionHandler,
				final TA taskAction, final TC taskCallback) {
			super(presenter, taskActionHandler, taskAction, taskCallback);
		}

		@Override
		protected void executeCall() {
			int retry = 0;
			while (retry < maxRetry) {
				try {
					super.executeCall();
					return;
				} catch (final Throwable th) {
					log.warn("Error while executing task [retry={}, error={}]", retry, th.getMessage());
					waitForRetry(retry);
					++retry;
				}
			}
			throw new RuntimeException("Can't execute RetryTaskRunner");
		}

		private static final Logger log = LogCatLogger.getLogger(RetryTaskRunner.class);

		private void waitForRetry(final int retry) {
			try {
				synchronized (this) {
					wait(retryTimeouts[retry]);
				}
			} catch (final InterruptedException e) {
			}
		}

		private final int maxRetry = 3;

		private final int[] retryTimeouts = { 100, 500, 1000 };

	}

	private <TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>> void execute(
			final TA taskAction, final TC taskCallback, final boolean retry) {
		try {
			final TaskActionHandler<TA, TR> taskActionHandler = getActionHandler(taskAction);
			executeTaskActionHandler(taskAction, taskCallback, taskActionHandler, retry);
		} catch (final Throwable th) {
			log.error("Can't instantionate TaskHandler instance [{}]", th.getMessage());
			taskCallback.onFailure(th);
			return;
		}
	}

	@SuppressWarnings("unchecked")
	// SuppressWarnings: we have map of (? extends TaskAction) == (TA) and (?
	// extends TaskResult) == (TR)
	private <TA extends TaskAction, TR extends TaskResult> TaskActionHandler<TA, TR> getActionHandler(
			final TA taskAction) throws InstantiationException, IllegalAccessException {
		final Class<? extends TaskActionHandler<? extends TaskAction, ? extends TaskResult>> taskActionHandlerClass = taskActionHandlers
				.get(taskAction.getClass());
		final TaskActionHandler<? extends TaskAction, ? extends TaskResult> taskActionHandler = Injector.getInjector(
				context).getInstance(taskActionHandlerClass);
		return (TaskActionHandler<TA, TR>) taskActionHandler;
	}

	private <TR extends TaskResult, TA extends TaskAction, TC extends TaskCallback<TR>> void executeTaskActionHandler(
			final TA taskAction, final TC taskCallback, final TaskActionHandler<TA, TR> taskActionHandler,
			final boolean retry) {
		final TaskRunner<TA, TR, TC> taskRunner = getTaskRunner(taskAction, taskCallback, taskActionHandler, retry);
		taskRunner.execute();
	}

	private <TA extends TaskAction, TC extends TaskCallback<TR>, TR extends TaskResult> TaskRunner<TA, TR, TC> getTaskRunner(
			final TA taskAction, final TC taskCallback, final TaskActionHandler<TA, TR> taskActionHandler,
			final boolean retry) {
		if (retry) {
			return new RetryTaskRunner<TA, TR, TC>(context, taskActionHandler, taskAction, taskCallback);
		} else {
			return new TaskRunner<TA, TR, TC>(context, taskActionHandler, taskAction, taskCallback);
		}
	}

	private final Map<Class<? extends TaskAction>, Class<? extends TaskActionHandler<? extends TaskAction, ? extends TaskResult>>> taskActionHandlers;

	private final Activity context;

}
