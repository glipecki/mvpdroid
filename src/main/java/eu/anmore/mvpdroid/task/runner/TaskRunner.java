package eu.anmore.mvpdroid.task.runner;

import eu.anmore.mvpdroid.async.TaskAction;
import eu.anmore.mvpdroid.async.TaskCallback;
import eu.anmore.mvpdroid.async.TaskResult;

/**
 * @author Grzegorz Lipecki
 */
public interface TaskRunner {

	<TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>> void execute(TA taskAction,
			TC taskCallback);

	<TA extends TaskAction, TR extends TaskResult, TC extends TaskCallback<TR>> void executeWithRetry(TA taskAction,
			TC taskCallback);

}
