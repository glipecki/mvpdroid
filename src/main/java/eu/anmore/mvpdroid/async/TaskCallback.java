package eu.anmore.mvpdroid.async;

/**
 * @author Grzegorz Lipecki
 */
public interface TaskCallback<T extends TaskResult> {

	void onSuccess(T taskResult);

	void onFailure(Throwable caught);

}
