package eu.anmore.mvpdroid.injector;

import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;
import android.content.Context;

import com.google.inject.ConfigurationException;

/**
 * @author Grzegorz Lipecki
 */
public class Injector {

	public static synchronized Injector getInjector(final Context context) {
		return new Injector(context);
	}

	public <T> T getInstance(final Class<T> type) {
		return roboGuiceInjector.getInstance(type);
	}

	public <T> T getOptionallyInstance(final Class<T> type) {
		try {
			return roboGuiceInjector.getInstance(type);
		} catch (final ConfigurationException configurationException) {
			return null;
		}
	}

	private Injector(final Context context) {
		this.roboGuiceInjector = RoboGuice.getInjector(context);
	}

	private final RoboInjector roboGuiceInjector;

}
