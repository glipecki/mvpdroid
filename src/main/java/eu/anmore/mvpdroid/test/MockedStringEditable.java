package eu.anmore.mvpdroid.test;

import android.text.Editable;
import android.text.SpannableStringBuilder;

/**
 * @author Grzegorz Lipecki
 */
public class MockedStringEditable extends SpannableStringBuilder implements Editable {

	public MockedStringEditable(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	private final String text;

}
