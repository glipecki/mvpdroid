package eu.anmore.mvpdroid.toast;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import roboguice.inject.ContextSingleton;
import android.app.Activity;

import com.google.inject.Inject;

/**
 * @author Grzegorz Lipecki
 */
@ContextSingleton
public class DefaultAndroidToast implements AndroidToast {

	@Inject
	public DefaultAndroidToast(final Activity context) {
		this.context = context;
	}

	@Override
	public void showShortToast(final int toastMessage) {
		makeText(context, toastMessage, LENGTH_SHORT).show();
	}

	@Override
	public void showShortToast(final String toastMessage) {
		makeText(context, toastMessage, LENGTH_SHORT).show();
	}

	private final Activity context;

}
