package eu.anmore.mvpdroid.async;

/**
 * @author Grzegorz Lipecki
 */
public interface TaskActionHandler<TA extends TaskAction, TR extends TaskResult> {

	TR handle(TA taskAction);

}
