package eu.anmore.mvpdroid.handler;

/**
 * @author Grzegorz Lipecki
 */
public interface ButtonClickHandler {

	void onClick();

}
