package eu.anmore.mvpdroid.view;

import eu.anmore.mvpdroid.presenter.ActivityPresenter;
import eu.anmore.mvpdroid.presenter.ActivityProxy;

/**
 * @author Grzegorz Lipecki
 */
public interface View {

	void onCreate();

	void setContext(ActivityProxy<? extends ActivityPresenter<? extends View>, ? extends View> activityProxy);

}
