package eu.anmore.mvpdroid.widget.builder;

import android.app.Activity;
import android.widget.TableLayout;

public final class TableLayoutBuilder extends ViewBuilder<TableLayout> {

	public TableLayoutBuilder(final Activity context) {
		super(context, TableLayout.class);
	}

}