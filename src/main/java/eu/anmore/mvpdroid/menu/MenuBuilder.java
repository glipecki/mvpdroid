package eu.anmore.mvpdroid.menu;

import android.view.Menu;
import android.view.MenuInflater;

/**
 * @author Grzegorz Lipecki
 */
public interface MenuBuilder {

	boolean onCreateOptionsMenu(Menu menu, MenuInflater menuInflater);

}
