package eu.anmore.mvpdroid.handler.listener;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * @author Grzegorz Lipecki
 */
public abstract class TextChangedListener implements TextWatcher {

	@Override
	public void afterTextChanged(final Editable editable) {
		// intentionally left blank.
	}

	@Override
	public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after) {
		// intentionally left blank.
	}

	@Override
	public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count) {
		// intentionally left blank.
	}

}