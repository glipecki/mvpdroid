package eu.anmore.mvpdroid.presenter;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.gson.Gson;
import com.google.inject.Inject;

import eu.anmore.mvpdroid.view.View;

/**
 * @author Grzegorz Lipecki
 */
public abstract class BasePresenter<VIEW_TYPE extends View> implements ActivityPresenter<VIEW_TYPE> {

	@Inject
	public BasePresenter(final VIEW_TYPE view) {
		this.view = view;
		this.gson = new Gson();
	}

	@Override
	public void setProxy(final ActivityProxy<? extends ActivityPresenter<VIEW_TYPE>, VIEW_TYPE> activityProxy) {
		this.activityProxy = activityProxy;
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
		// be default don't show menu.
		return false;
	}

	@Override
	public boolean onHomeButtonClick() {
		return onHomeButtonClickedListener.onHomeButtonClick();
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		// intentionally left blank.
	}

	/**
	 * @return if presenter prevents default back button handling
	 */
	@Override
	public boolean onBackPressed() {
		return false;
	}

	@Override
	public void onConfigurationChanged(final Configuration newConfig) {
		// intentionally left blank.
	}

	@Override
	public void onRestart() {
		// intentionally left blank.
	}

	@Override
	public void onStart() {
		// intentionally left blank.
	}

	@Override
	public void onResume() {
		// intentionally left blank.
	}

	@Override
	public void onPause() {
		// intentionally left blank.
	}

	@Override
	public void onNewIntent(final Intent intent) {
		// intentionally left blank.
	}

	@Override
	public void onStop() {
		// intentionally left blank.
	}

	@Override
	public void onDestroy() {
		// intentionally left blank.
	}

	@Override
	public void onContentChanged() {
		// intentionally left blank.
	}

	@Override
	public VIEW_TYPE getView() {
		return view;
	}

	@Override
	public Intent getIntent() {
		return activityProxy.getIntent();
	}

	@Override
	public ActivityProxy<? extends ActivityPresenter<VIEW_TYPE>, VIEW_TYPE> getProxy() {
		return activityProxy;
	}

	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		// intentionally left blank.
	}

	protected void setOnHomeButtonClickedListener(final OnHomeButtonClickedListener onHomeButtonClickedListener) {
		this.onHomeButtonClickedListener = onHomeButtonClickedListener;
	}

	protected <T> T getIntentParameter(final String key, final Class<T> type) {
		final Intent intent = getIntent();
		if (!intent.getExtras().containsKey(key)) {
			// TODO (glipecki): error
		}

		final String rawValue = (String) intent.getExtras().get(key);
		return gson.fromJson(rawValue, type);
	}

	private final Gson gson;

	private ActivityProxy<? extends ActivityPresenter<VIEW_TYPE>, VIEW_TYPE> activityProxy;

	private final VIEW_TYPE view;

	private OnHomeButtonClickedListener onHomeButtonClickedListener = new NoActionOnHomeButtonClicked();

}
