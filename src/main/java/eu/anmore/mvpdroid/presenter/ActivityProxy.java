package eu.anmore.mvpdroid.presenter;

import java.lang.reflect.Field;
import java.util.Map;

import roboguice.RoboGuice;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboContext;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.inject.Key;

import eu.anmore.mvpdroid.injector.Injector;
import eu.anmore.mvpdroid.menu.MenuBuilder;
import eu.anmore.mvpdroid.proxy.ScopedObjectMap;
import eu.anmore.mvpdroid.view.View;

/**
 * todo: pass proxy reference to newly instantiate presenter.
 * 
 * @author Grzegorz Lipecki
 */
public class ActivityProxy<PRESENTER_TYPE extends ActivityPresenter<VIEW_TYPE>, VIEW_TYPE extends View> extends
		Activity implements RoboContext {

	public ActivityProxy(final Class<? extends PRESENTER_TYPE> presenterClass) {
		this.presenterClass = presenterClass;
	}

	/** RoboGuice dependency. */
	@Override
	public Map<Key<?>, Object> getScopedObjectMap() {
		return scopedObjects;
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		presenter = getPresenter();
		setContentViewIfAnnotationExists();
		injectViewMembers();
		presenter.onCreate(savedInstanceState);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return presenter.onHomeButtonClick();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		if (!presenter.onBackPressed()) {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final boolean menuBuildedByCommonMenuBuilder = buildMenuByCommonBuilderIfExists(menu);
		final boolean menuBuildedByPresenter = presenter.onCreateOptionsMenu(menu, getMenuInflater());

		return menuBuildedByCommonMenuBuilder || menuBuildedByPresenter;
	}

	@Override
	public void onConfigurationChanged(final Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		presenter.onConfigurationChanged(newConfig);
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
		injectViewMembers();
		presenter.onContentChanged();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		presenter.onRestart();
	}

	@Override
	protected void onStart() {
		super.onStart();
		presenter.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		presenter.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		presenter.onPause();
	}

	@Override
	protected void onNewIntent(final Intent intent) {
		super.onNewIntent(intent);
		presenter.onNewIntent(intent);
	}

	@Override
	protected void onStop() {
		try {
			presenter.onStop();
		} finally {
			super.onStop();
		}
	}

	@Override
	protected void onDestroy() {
		try {
			presenter.onDestroy();
		} finally {
			try {
				RoboGuice.destroyInjector(this);
			} finally {
				super.onDestroy();
			}
		}
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		presenter.onActivityResult(requestCode, resultCode, data);
	}

	protected ScopedObjectMap scopedObjects = new ScopedObjectMap();

	private boolean buildMenuByCommonBuilderIfExists(final Menu menu) {
		final MenuBuilder commonMenuBuilder = getInjector().getOptionallyInstance(MenuBuilder.class);
		if (commonMenuBuilder != null) {
			return commonMenuBuilder.onCreateOptionsMenu(menu, getMenuInflater());
		}
		return false;
	}

	private void setContentViewIfAnnotationExists() {
		final Class<? extends View> presenterViewClass = getViewClass();
		if (presenterViewClass.isAnnotationPresent(ContentView.class)) {
			setContentView(presenterViewClass.getAnnotation(ContentView.class).value());
		}
	}

	private void injectViewMembers() {
		final Class<? extends View> viewClass = getViewClass();
		final Field[] fields = viewClass.getDeclaredFields();
		for (final Field field : fields) {
			if (field.isAnnotationPresent(InjectView.class)) {
				final InjectView injectViewAnnotation = field.getAnnotation(InjectView.class);
				final int viewToInjectId = injectViewAnnotation.value();
				if (viewToInjectId != -1) {
					try {
						field.setAccessible(true);
						field.set(presenter.getView(), findViewById(viewToInjectId));
					} catch (final Throwable e) {
						throw new RuntimeException("Can't injevt view [" + field.getName() + "]", e);
					}
				}
			}
		}
		presenter.getView().setContext(presenter.getProxy());
		presenter.getView().onCreate();
	}

	private Class<? extends View> getViewClass() {
		return presenter.getView().getClass();
	}

	private PRESENTER_TYPE getPresenter() {
		final PRESENTER_TYPE presenter = getInjector().getInstance(presenterClass);
		presenter.setProxy(this);
		return presenter;
	}

	private Injector getInjector() {
		return Injector.getInjector(this);
	}

	private final Class<? extends PRESENTER_TYPE> presenterClass;

	private PRESENTER_TYPE presenter;

}
