package eu.anmore.mvpdroid.presenter;

/**
 * @author Grzegorz Lipecki
 */
public interface OnHomeButtonClickedListener {

	public boolean onHomeButtonClick();

}
