package eu.anmore.mvpdroid.presenter;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import eu.anmore.mvpdroid.view.View;

public interface ActivityPresenter<VIEW_TYPE extends View> {

	boolean onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater);

	void onCreate(final Bundle savedInstanceState);

	void onConfigurationChanged(final Configuration newConfig);

	boolean onHomeButtonClick();

	/**
	 * @return if presenter prevents default back button handling
	 */
	boolean onBackPressed();

	void onRestart();

	void onStart();

	void onResume();

	void onPause();

	void onNewIntent(final Intent intent);

	void onStop();

	void onDestroy();

	void onContentChanged();

	void onActivityResult(final int requestCode, final int resultCode, final Intent data);

	VIEW_TYPE getView();

	Intent getIntent();

	ActivityProxy<? extends ActivityPresenter<VIEW_TYPE>, VIEW_TYPE> getProxy();

	void setProxy(final ActivityProxy<? extends ActivityPresenter<VIEW_TYPE>, VIEW_TYPE> activityProxy);

}