package eu.anmore.mvpdroid.widget.builder;

import android.app.Activity;

public final class WidgetBuilder {

	public WidgetBuilder(final Activity context) {
		this.context = context;
	}

	public TextViewBuilder textView() {
		return new TextViewBuilder(context);
	}

	public TableRowBuilder tableRow() {
		return new TableRowBuilder(context);
	}

	public TableLayoutBuilder table() {
		return new TableLayoutBuilder(context);
	}

	public CheckBoxBuilder checkBox() {
		return new CheckBoxBuilder(context);
	}

	private final Activity context;

}