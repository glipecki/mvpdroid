package eu.anmore.mvpdroid.view;

import eu.anmore.mvpdroid.presenter.ActivityPresenter;
import eu.anmore.mvpdroid.presenter.ActivityProxy;

/**
 * @author Grzegorz Lipecki
 */
public class BaseView implements View {

	@Override
	public void onCreate() {
	}

	@Override
	public void setContext(
			final ActivityProxy<? extends ActivityPresenter<? extends View>, ? extends View> activityProxy) {
		this.context = activityProxy;
	}

	public ActivityProxy<? extends ActivityPresenter<? extends View>, ? extends View> getContext() {
		return context;
	}

	private ActivityProxy<? extends ActivityPresenter<? extends View>, ? extends View> context;

}
