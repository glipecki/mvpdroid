package eu.anmore.mvpdroid.widget.builder;

import android.app.Activity;
import android.widget.CheckBox;

public final class CheckBoxBuilder extends ViewBuilder<CheckBox> {

	public CheckBoxBuilder(final Activity context) {
		super(context, CheckBox.class);
	}

	public CheckBoxBuilder checked(final boolean checked) {
		object.setChecked(checked);
		return this;
	}

}