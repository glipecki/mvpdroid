package eu.anmore.mvpdroid.presenter;

/**
 * @author Grzegorz Lipecki
 */
public class NoActionOnHomeButtonClicked implements OnHomeButtonClickedListener {

	@Override
	public boolean onHomeButtonClick() {
		return false;
	}

}
