package eu.anmore.mvpdroid.view;

import android.view.View;

/**
 * @author Grzegorz Lipecki
 */
public interface ListItemView {

	View getView();

}
