package eu.anmore.mvpdroid.widget.builder;

import android.app.Activity;
import android.text.Html;
import android.widget.TextView;

public final class TextViewBuilder extends ViewBuilder<TextView> {

	public TextViewBuilder(final Activity context) {
		super(context, TextView.class);
	}

	public TextViewBuilder textFromHtml(final String htmlText) {
		text(Html.fromHtml(htmlText));
		return this;
	}

	public TextViewBuilder text(final CharSequence text) {
		object.setText(text);
		return this;
	}

	public TextViewBuilder gravity(final int gravity) {
		object.setGravity(gravity);
		return this;
	}

	public TextViewBuilder textSize(final int size) {
		object.setTextSize(size);
		return this;
	}

	public TextViewBuilder text(final int textRef) {
		object.setText(textRef);
		return this;
	}

	public TextViewBuilder padding(final int left, final int top, final int right, final int bottom) {
		object.setPadding(left, top, right, bottom);
		return this;
	}

	public TextViewBuilder height(final int height) {
		object.setHeight(height);
		return this;
	}

}