package eu.anmore.mvpdroid.toast;

/**
 * @author Grzegorz Lipecki
 */
public interface AndroidToast {

	void showShortToast(int toastMessage);

	void showShortToast(String toastMessage);

}
