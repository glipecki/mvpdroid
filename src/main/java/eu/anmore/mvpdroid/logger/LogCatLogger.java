package eu.anmore.mvpdroid.logger;

import android.util.Log;

/**
 * Android Logger.
 * 
 * @author Grzegorz Lipecki
 */
public class LogCatLogger implements Logger {

	public static LogCatLogger getLogger(final Class<?> clazz) {
		return new LogCatLogger(clazz.getName());
	}

	@Override
	public void info(final String message) {
		try {
			Log.i(className, message);
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void info(final String message, final Object... params) {
		try {
			Log.i(className, buildMessage(message, params));
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void debug(final String message) {
		try {
			Log.d(className, message);
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void debug(final String message, final Object... params) {
		try {
			Log.d(className, buildMessage(message, params));
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void trace(final String message) {
		try {
			Log.v(className, message);
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void trace(final String message, final Object... params) {
		try {
			Log.v(className, buildMessage(message, params));
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void warn(final String message) {
		try {
			Log.w(className, message);
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void warn(final String message, final Object... params) {
		try {
			Log.w(className, buildMessage(message, params));
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void error(final String message) {
		try {
			Log.e(className, message);
		} catch (final RuntimeException e) {
		}
	}

	@Override
	public void error(final String message, final Object... params) {
		try {
			Log.e(className, buildMessage(message, params));
		} catch (final RuntimeException e) {
		}
	}

	private static final String VARIABLE_HOLDER_PATTERN = "\\{\\}";

	private static final String VARIABLE_HOLDER = "{}";

	private LogCatLogger(final String className) {
		this.className = className;
	}

	private String buildMessage(String message, final Object... params) {
		int variableToPutIndex = 0;
		while (containsVariableHolder(message)) {
			message = message.replaceFirst(VARIABLE_HOLDER_PATTERN, String.valueOf(params[variableToPutIndex]));
			variableToPutIndex++;
		}
		return message;
	}

	private boolean containsVariableHolder(final String message) {
		return message.contains(VARIABLE_HOLDER);
	}

	private final String className;

}
