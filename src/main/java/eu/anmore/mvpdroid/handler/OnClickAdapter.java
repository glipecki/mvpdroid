package eu.anmore.mvpdroid.handler;

import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author Grzegorz Lipecki
 */
public class OnClickAdapter implements OnClickListener {

	public OnClickAdapter(final ButtonClickHandler buttonClickHandler) {
		this.buttonClickHandler = buttonClickHandler;
	}

	@Override
	public void onClick(final View v) {
		buttonClickHandler.onClick();
	}

	private final ButtonClickHandler buttonClickHandler;

}
