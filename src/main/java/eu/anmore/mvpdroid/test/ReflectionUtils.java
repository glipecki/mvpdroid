package eu.anmore.mvpdroid.test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Grzegorz Lipecki
 */
public class ReflectionUtils {

	public static void setField(final Object object, final String name, final Object value) {
		try {
			getInheritedField(object.getClass(), name).set(object, value);
		} catch (final Throwable th) {
			throw new RuntimeException(th);
		}
	}

	public static Field getInheritedField(final Class<?> type, final String name) {
		for (final Field f : getInheritedFields(type)) {
			if (f.getName().equals(name)) {
				f.setAccessible(true);
				return f;
			}
		}
		return null;
	}

	public static List<Field> getInheritedFields(final Class<?> type) {
		final List<Field> fields = new ArrayList<Field>();
		for (Class<?> c = type; c != null; c = c.getSuperclass()) {
			fields.addAll(Arrays.asList(c.getDeclaredFields()));
		}
		return fields;
	}

}
