package eu.anmore.mvpdroid.test;

import eu.anmore.mvpdroid.view.View;

/**
 * @author Grzegorz Lipecki
 */
public class ViewMockBuilder<T extends View> {

	public static <T extends View> ViewMockBuilder<T> get(final T view) {
		return new ViewMockBuilder<T>(view);
	}

	public ViewMockBuilder<T> withToast(final Object object) {
		ReflectionUtils.setField(view, TOAST_FIELD_NAME, object);
		return this;
	}

	public ViewMockBuilder<T> with(final String name, final Object object) {
		ReflectionUtils.setField(view, name, object);
		return this;
	}

	public T build() {
		return view;
	}

	private ViewMockBuilder(final T view) {
		this.view = view;
	}

	private static final String TOAST_FIELD_NAME = "toast";

	private final T view;

}