package eu.anmore.mvpdroid.widget.builder;

import java.lang.reflect.InvocationTargetException;

import android.app.Activity;
import android.view.View;

public abstract class ViewBuilder<T extends View> {

	public ViewBuilder(final Activity context, final Class<T> type) {
		try {
			this.object = type.getConstructor(android.content.Context.class).newInstance(context);
		} catch (final IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (final InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (final NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}

	public T get() {
		return object;
	}

	protected final T object;

}